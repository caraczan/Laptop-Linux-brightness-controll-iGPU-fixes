# Different Languages
- [Polski](README_pl.md)
- [English](README.md)

# Backlight control not working on laptop with Ryzen 7 4700U and iGPU
I have bought recently new laptop and immeditly went to install Linux distro on it.
Unfortunatly I've encourted problems first with Linux liveOS not booting at all, then with installed OS not booting and then with brightness cocntrol not working.

In my journey to find correct answer I have found that there are a lot more peoply having problems similar or the same as I had.

So I've decided to compile fixes for three problems I've encountered.

# Laptop
I've bought Hyperbook R15 Ryzen 7 4700U without RAM and SSD, added my own.

* [Hyperbook R15](https://www.hyperbook.pl/sklep/pl/laptopy/301-hyperbook-r15-zen-ryzen-7-4700u.html?search_query=R15&results=2)
	* CPU: AMD Ryzen 7 4700U
	* GPU: Integrated Vega 7
	* RAM: GOODRAM 1x8GB 3200MHz
	* SSD: GOODRAM IRDM 512GB NVMe 4x3Gen  
	* design by CLEVO

# Linux
I've installed [ArcolinuxB](https://www.arcolinux.info/downloads/) with KDE Plasma on board. 

# Issues

## 1. Hanging on boot
* From unconfirmed sources(internet forums) this issue is either fault of AMD code or AMD motherboards partners.

### liveboot fix
1. (a) during grub screen select boot that is described as `safe` or `no gpu mode`, if not
1. (b) during grub screen select boot you want to choose, hit `e`,
2. on the bottom of the screen new line will show up, go to the end of it,
3. add ` nomodeset iommu=off`,
4. complete installation,

### boot(permament) fix(after installation complete)
5. repeat step `1. (b)`,
6. find and go to line starting with `linux`, 
7. add `splash nomoedset iommu=off` so the end result will look like `linux ... quiet splash nomoedset iommu=off ...`
8. boot by hiting `F10` or `ctrl+x`,
9. after login open terminal and enter `sudo vim /etc/defaults/grub`, (any editor will do, you don't have to use `vim`)
10. on line `GRUB_CMDLINE_LINUX_DEFAULT` add `quiet splash iommu=soft ...` => `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash iommu=soft ..."`,
11. save changes and leave editor,
12. update grub with `sudo grub-mkconfig -o /boot/grub/grub.cfg` (on Arch-based),
13. reboot.


## 2. Black screen past login
* Sometimes this happen when packages version are mixed or just old, perhaps dependency broke during instaltion.

### fix
1. `ctrl+alt+F2` to go to tty2 
2. log in in cli with created user
3. log in to `wifi` or `ethernet` using prefered tool, in my case I was using `nmcli` from `NetworkManager` to connect to `wifi`
4. run `sudo pacman -Syu` to update system, prepare for 1~3 GB of packages,
5. reboot with `sudo reboot`,


## 3. Cannot control bakclight

### fix
1. in loged in system open terminal,
2. check `sudo systemctl status systemd-backlight@backlight:acpi_video0.service`, service should be failed
3. if it's failed check `journal -b _PID=<PID_from_above_output>`, somethin along `Failed to get backlight or LED device 'backlight:acpi_video0': No such device` should appear, 

### actual fix
4. enter `sudo vim /etc/defaults/grub` (or any other editor)
5. change line `GRUB_CMDLINE_LINUX_DEFAULT` by adding `acpi_backlight=vendor` after `quiet` before `splash` , should be `quiet acpi_backlight=vendor splash...`, save changes and leave,
6. update grub with `sudo grub-mkconfig -o /boot/grub/grub.cfg` (on Arch-based),
7. reboot.

---

## Post Scriptum

I hope those fixes will help anyone.

---
# Sources:
* https://bbs.archlinux.org/viewtopic.php?id=259892
* https://bbs.archlinux.org/viewtopic.php?id=211967

## additional sources:
* https://unix.stackexchange.com/questions/111889/how-do-i-update-grub-in-arch-linux
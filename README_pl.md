# Inne Języki
- [Polski](README_pl.md)
- [English](README.md)

# Nie działająca kontrola podświetlenia ekranu w laptopach z procesorem Ryzen 7 4700U i z iGPU
Nie dawno kupiłem nowy laptop, od razu(po zamontowaniu podzespołów) zajołem się instalacją systeu Linuks.
Niestety pierwszy problem wyszedł przy próbie odpalenia Linuksa wersji live. Po rozwiązaniu tego problemu napotkałem jeszcze dwa inne z m.i. niedziałającą kontrolą jasności.

Szukając rozwiązania napotkałem wiele podobnych pytań na różnych forach i nie zawsze działające odpowiedzi. 

Aby ułatwić rozwiązanie problemów innym opisałem kroki do rozwiązania tych błedów.

# Laptop
Kupiłem Hyperbook R15 Ryzen 7 4700U bez pamięci operacyjnej(RAM) i dysku twardego(SSD M.2 NVME)

* [Hyperbook R15](https://www.hyperbook.pl/sklep/pl/laptopy/301-hyperbook-r15-zen-ryzen-7-4700u.html?search_query=R15&results=2)
	* CPU: AMD Ryzen 7 4700U
	* GPU: Zintegrowana Vega 7
	* RAM: GOODRAM 1x8GB 3200MHz
	* SSD: GOODRAM IRDM 512GB NVMe 4x3Gen  
	* zaprojektowane przez CLEVO

# Linuks
Zainstalowałem [ArcolinuxB](https://www.arcolinux.info/downloads/) z KDE Plasma. 

# Problemy/błędy

## 1. Zawieszenie podczas uruchamiani(ładowania kernela)
* Z niepotwiedzonych źródeł(fora internetowe) jest to problem albo od firmy AMD albo od partnerów AMD płyt głównych.

### liveboot fix
1. (a) during grub screen select boot that is described as `safe` or `no gpu mode`, if not
1. (b) during grub screen select boot you want to choose, hit `e`,
2. on the bottom of the screen new line will show up, go to the end of it,
3. add ` nomodeset iommu=off`,
4. complete installation,

### boot(permament) fix(after installation complete)
5. repeat step `1. (b)`,
6. find and go to line starting with `linux`, 
7. add `splash nomoedset iommu=off` so the end result will look like `linux ... quiet splash nomoedset iommu=off ...`
8. boot by hiting `F10` or `ctrl+x`,
9. after login open terminal and enter `sudo vim /etc/defaults/grub`, (any editor will do, you don't have to use `vim`)
10. on line `GRUB_CMDLINE_LINUX_DEFAULT` add `quiet splash iommu=soft ...` => `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash iommu=soft ..."`,
11. save changes and leave editor,
12. update grub with `sudo grub-mkconfig -o /boot/grub/grub.cfg` (on Arch-based),
13. reboot.


## 2. Black screen past login
* Sometimes this happen when packages version are mixed or just old, perhaps dependency broke during instaltion.

### fix
1. `ctrl+alt+F2` to go to tty2 
2. log in in cli with created user
3. log in to `wifi` or `ethernet` using prefered tool, in my case I was using `nmcli` from `NetworkManager` to connect to `wifi`
4. run `sudo pacman -Syu` to update system, prepare for 1~3 GB of packages,
5. reboot with `sudo reboot`,


## 3. Cannot control bakclight

### fix
1. in loged in system open terminal,
2. check `sudo systemctl status systemd-backlight@backlight:acpi_video0.service`, service should be failed
3. if it's failed check `journal -b _PID=<PID_from_above_output>`, somethin along `Failed to get backlight or LED device 'backlight:acpi_video0': No such device` should appear, 

### actual fix
4. enter `sudo vim /etc/defaults/grub` (or any other editor)
5. change line `GRUB_CMDLINE_LINUX_DEFAULT` by adding `acpi_backlight=vendor` after `quiet` before `splash` , should be `quiet acpi_backlight=vendor splash...`, save changes and leave,
6. update grub with `sudo grub-mkconfig -o /boot/grub/grub.cfg` (on Arch-based),
7. reboot.

---

## Post Scriptum

I hope those fixes will help anyone.

---
# Sources:
* https://bbs.archlinux.org/viewtopic.php?id=259892
* https://bbs.archlinux.org/viewtopic.php?id=211967

## additional sources:
* https://unix.stackexchange.com/questions/111889/how-do-i-update-grub-in-arch-linux